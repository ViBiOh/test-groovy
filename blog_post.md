
## Vos scripts sous stéroïdes avec Groovy ##
Au cours du cycle de vie d'une application, il y a beaucoup de besoins nécessitant la réalisation de scripts : 

 - faire un backup
 - réaliser des opérations d'administration en ligne de commande
 - exécuter des actions de maintenance
 - effectuer des mises à jour sur des modèles de bases de données
 - etc, .... 

Et il existe au moins tout autant de façon de réaliser ces scripts : 

 - en bash
 - en powershell (pour Windows)
 - en python
 - en PHP
 - en groovy

### Le choix de Groovy

Comme le titre de l'article l'indique, c'est mon expérience sur le scripting en [Groovy](http://groovy-lang.org/) que je souhaite vous faire partager. 

Groovy est un langage dynamique pour la JVM, supportant le typage dynamique et la compilation statique. Groovy apporte également pas mal d'améliorations appréciables par rapport à la syntaxe verbeuse de Java en s'inspirant de Ruby et Python. Il a également l'avantage d'être 100 % compatible avec la syntaxe de Java, ainsi un code Java est un code Groovy valide.

Le fait d'utiliser Groovy pour nos scripts va permettre de réaliser des scripts pour des environnements hétérogènes (windows et linux), de bénéficier de la richesse de l'écosystème Java et d'avoir des scripts plus faciles à lire et à maintenir, notammant grâce à l'apport des [Closures](http://www.groovy-lang.org/closures.html). 

Le cas d'utilisation qui nous servira d'exemple est un script qui va lire les titres des articles sur la page d'accueil de sites de presse et les importer dans une base de données. 

### Cas d'utilisation : récupérer les articles d'un site web

Pour récupérer les titres sur la page d'accueil d'un site de presse on utilisera la bibliothèque [Jsoup](http://jsoup.org/). Groovy est livré avec un gestionnaire de dépendances : [Grape](http://groovy-lang.org/grape.html) qui va nous permettre d'utiliser des dépendances externes dans un script Groovy. 

    // Load Jsoup used the get html pages and query them
	@Grab("org.jsoup:jsoup:1.8.2")
	import org.jsoup.Jsoup
	
	// Get the articles titles on the Slate home page
    println "Reading articles on Slate"
    def document = Jsoup.connect("http://slate.com/").get();
    def h1Elements = document.select("article h1")
    def titles = []
    h1Elements.each { titles.add(it.text()) }

	// Display the titles
	println ""
    println "Articles: "
    titles.each { println " - " + it }


### Cas d'utilisation : Charger des articles dans une base de données ###

JDBC peut être utilisé depuis le script groovy pour charger en base les données récupérées sur le site web. J'ai choisi d'utiliser [Neo4j](http://neo4j.com/) comme base de données (Graphe) mais cela aurait pu être toutes bases de données fournissant un driver JDBC. 

Groovy est fournit également avec un [module SQL](http://docs.groovy-lang.org/latest/html/api/groovy/sql/Sql.html) permettant de requêter une base de donnée en utilisant le driver idoine. L'[API JDBC](http://www.jmdoudoux.fr/java/dej/chap-jdbc.htm) classique peut toujours être utilisée. 

	// Load Neo4j JDBC to load data and query a Neo4j database
	@GrabConfig(systemClassLoader=true)
	@GrabResolver(name='neo4j-public', root='http://m2.neo4j.org/content/groups/public')
	@Grab("org.neo4j:neo4j-jdbc:2.1.4")
	import org.neo4j.jdbc.Driver
	
	import groovy.sql.Sql
	
	// Put the article titles into a database
	def sql = Sql.newInstance('jdbc:neo4j://localhost:7474/')
	titles.each {
	    def cypherQuery = '''
	        MERGE (w:`Website` {name: {1}})
	        MERGE (d:`Day` {date: {2}})
	        CREATE
	          (a:`Article` {3}),
	          (a)-[:`PUBLISHED_ON`]->(w),
	          (a)-[:`PUBLISHED_AT`]->(d)
	    '''
	    sql.execute(cypherQuery, [
	            "slate.com",
	            new Date().format("YYYY/MM/dd"),
	            [title:it]
	    ])
	    println "Inserted: ${it}"
	}

En utilisant le module Groovy SQL, il est nécessaire d'échapper les labels et relations Neo4j avec des "backticks"  `` ` `` sinon Groovy va interpréter les labels et types de relation comme des paramètres nommés. 

L'annotation `@GrabConfig(systemClassLoader=true)` est nécessaire afin que le driver puisse être correctement utilisé. 

Le script final peut être trouvé à via le Gist suivant : [https://gist.github.com/Leward/c93f6e3265a23119934f](https://gist.github.com/Leward/c93f6e3265a23119934f) 

### Exécuter le script sur le serveur ###

Exécuter le script côté serveur se révèle très simple. Il suffit en effet d'avoir le runtime groovy installé, d'envoyer le script `.groovy` sur le serveur et de l'exécuter avec la commande suivante : 

    groovy mon_script.groovy

Il est également possible d'utiliser la notation "Shebang" en indiquant au début du script : 

    #!/usr/bin/groovy

Il devient alors possible d'exécuter le script de la manière suivante : 

    ./mon_script.groovy

### Astuces avec IntelliJ ###
IntelliJ est capable de résoudre les dépendances indiquées dans les annotations  `@Grab` en plaçant le pointeur sur l'annotation et en utilisant le raccourcis `Alt + Entrée`. 

![Récupérer les artéfacts](intellij_grabe_artifacts.png)

Vous pourrez ainsi bénéficier de l'auto-complétion sur ces dépendances dans l'IDE. 

### Aller plus loin ###

 - Pour des scripts plus complexes il peut être plus intéressant de découper le script en plusieurs fichiers. Cela est possible en Groovy : "[Groovy scripting using multiple files](http://ikoodi.nl/2012/05/14/groovy-scripting-using-multiple-files/)"
 - Groovy permet de paralléliser vos traitements en utilisant [GPars](http://www.gpars.org/guide/guide/index.html). 
 - Pour installer un environnement Groovy vous pouvez utiliser "[GVM: the Groovy enVironment Manager](http://gvmtool.net/)"



